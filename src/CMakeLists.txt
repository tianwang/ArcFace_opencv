cmake_minimum_required (VERSION 2.6)

add_compile_options(-std=c++11)

include_directories(../inc)
link_directories(../lib/linux_x64)

project (arcsoft_afd_cv)

add_executable(arcsoft_afd_cv arcsoft_afd_samplecode.cpp)
target_link_libraries(arcsoft_afd_cv -larcsoft_fsdk_face_detection -lopencv_core -lopencv_highgui -lopencv_imgproc)